import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/my-files-view",
      name: "My File View",
      component: () => import("../views/MyFileView.vue"),
    },
    {
      path: "/shared-with-me",
      name: "Shared With Me",
      component: () => import("../views/SharedWithMe.vue"),
    },
    {
      path: "/starred-view",
      name: "Starred",
      component: () => import("../views/StarredView.vue"),
    },
  ],
});

export default router;
